function Dog(name, owner, color){
    this.name = name;
    this.color = color;
    this.owner = owner;
    this.setName = function(new_name){
        this.name = new_name;
    }



}

let gotze = new Dog('andoni','mbappi','white');
gotze.setName('goretzca');

/*Para agregar mas de una propiedad a nuestra funcion constructora 'Dog',
usame un objeto


*/

Dog.prototype={
    eat: function(){
        return 'Eating ñom ñom ñom';
    },

    ladrar: function(){
        return 'woow woow'
    },

    size: ''


}

//Cuando queremos agg de manera simple una propiedad a nuestro constructor
// pues...

Dog.prototype.setSize= function(new_size){
    this.size = new_size;

}

Dog.prototype.str= function(){
    return 'Nombre : ' + this.name + ' | Dueño: ' + this.owner + ' | Tamaño: ' + this.size;
}

let gory = new Dog('javi','drew','blue');

gory.setSize('Grande');

console.log(gory.str());
console.log(gory.ladrar());




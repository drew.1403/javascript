class Persona{

    static contadorPersona=0;

    constructor(nombre, apellido, edad){
        this._nombre = nombre;
        this._apellido = apellido;
        this._edad = edad;
        this._idPersona = ++Persona.contadorPersona;

    }

    get getidPersona(){
        return this._idPersona;

    }

    get getNombre(){
        return this._nombre;

    }

    set setNombre(new_name){
        this._nombre = new_name;
        
    }

    get getApellido(){
        return this._apellido;

    }

    set setApellido(new_apellido){
        this._apellido = new_apellido;
        
    }

    get getEdad(){
        return this._edad;

    }

    set setEdad(new_edad){
        this._edad = new_edad;
        
    }

    toString(){
        return `Nombre : ${this._nombre} | Apellido: ${this._apellido} | Edad: ${this._edad} | id: ${this._idPersona}`;
    }


}

class Empleado extends Persona{

    constructor(sueldo,nombre, apellido, edad){
        super(nombre, apellido, edad);
        this._sueldo = sueldo;
        this._idEmpleado = Empleado.contadorPersona;

    }

    get getIdEmpleado(){
        return this._idEmpleado;
    }

    get getSueldo(){
        return this._sueldo;

    }

    set setSueldo(new_sueldo){
        this._sueldo = new_sueldo;
    
    }

    toString(){
        return super.toString() + `
        Sueldo: ${ this.getSueldo }`
    }

   

}
class Cliente extends Persona{

    static contadorCliente=0;

    constructor(nombre, apellido, edad){
        super(nombre, apellido, edad)
        this._fechaRegistro = new Date();
        this._idCliente = ++Cliente.contadorCliente;

    }

    get getIdCliente(){
        return this._idCliente;
    }

    get getFechaRegistro(){
        return this._fechaRegistro;
    }

    set setFechaRegistro(new_date){
        this._fechaRegistro = new_date;
    }

    toString(){
        return `Nombre : ${this._nombre} | Apellido: ${this._apellido} | Edad: ${this._edad} | id: ${this._idCliente} `;
    }
}

let p1 = new Persona('javi','hernades',23);
let p2 = new Persona('ousmane','dembele',21);
console.log(p1.toString());

let e1 = new Empleado(2300,'lola','perez',12);

let c1 = new Cliente('sami','perez',23);
console.log(c1.toString());
console.log(e1.toString());






'use strict';

const fs = require('fs');

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', function(inputStdin) {
    inputString += inputStdin;
});

process.stdin.on('end', function() {
    inputString = inputString.split('\n');

    main();
});

function readLine() {
    return inputString[currentLine++];
}

/*
 * Complete the 'diagonalDifference' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts 2D_INTEGER_ARRAY arr as parameter.
 */

function diagonalDifference(arr) {
    // Write your code here
    let diagonal = 0;
    let diagonal_inversa=0;
    let resultado = 0;

    for (let i = 0, j = arr.length  - 1; i < arr.length; i++, j--) {
        diagonal += arr[i][i];
        diagonal_inversa +=  arr[i][j];
  
    }

    resultado = diagonal - diagonal_inversa;
    if (resultado < 0 ) {
        return resultado*=-1;
        
    }

    return resultado;
    

}

function main() {
    const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

    const n = parseInt(readLine().trim(), 10);

    let arr = Array(n);

    for (let i = 0; i < n; i++) {
        arr[i] = readLine().replace(/\s+$/g, '').split(' ').map(arrTemp => parseInt(arrTemp, 10));
    }

    const result = diagonalDifference(arr);

    ws.write(result + '\n');

    ws.end();
}


const arr = [
    [1, 2, 3],
    [3, 4, 3],
    [5, 6, -8]
];
  



const result = diagonalDifference(arr);

console.log(result);


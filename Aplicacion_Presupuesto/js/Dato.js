class Dato{
    constructor(descripcion,valor){
        this._descripcion = descripcion;
        this._valor = valor;

    }

    get descripcion(){
        return this._descripcion;
    }

    set descripcion(new_desc){
        this._descripcion = new_desc;
    }

    get valor(){
        return this._valor;
    }

    set valor(new_val){
        this._valor = new_val;
    }
}
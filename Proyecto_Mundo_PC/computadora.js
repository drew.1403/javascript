class Computadora{
    static contadorComputadoras=0;
    constructor(nombre,monitor,teclado,raton){
        this._idComputadora = ++Computadora.contadorComputadoras;
        this._nombre = nombre;
        this._monitor = monitor;
        this._teclado = teclado;
        this._raton = raton;

    }

    get getIdComputadora(){
        return this._idComputadora;
    }

    get getNombre(){
        return this._nombre;

    }

    set setNombre(new_name){
        this._nombre = new_name;
    }

    get getMonitor(){
        return this._monitor;
    }

    set setMonitor(new_monitor){
        this._monitor = new_monitor;
    }

    get getTeclado(){
        return this._teclado;
    }

    set setTeclado(new_teclado){
        this._teclado = new_teclado;
    }

    get getRaton(){
        return this._raton;
    }

    set setRaton(new_raton){
        this._raton = new_raton;
    }

    toString(){
        return `Computadora " ${this._nombre} " ID: ${this._idComputadora} \n\nMONITOR: ${this._monitor}\nTECLADO: ${this._teclado}\nRATON: ${this._raton}`;
    }
}
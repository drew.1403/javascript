class DispositivoEntrada{
    constructor(tipoEntrada,marca){
        this._tipoEntrada = tipoEntrada;
        this._marca = marca;
    }

    get getTipoEntrada(){
        return this._tipoEntrada;
    }

    set setTipoEntrada(new_tipo){
        this._tipoEntrada = new_tipo;
    }

    get getMarca(){
        return this._marca;
    }

    set setMarca(new_marca){
        this._marca = new_marca;
    }
}

class Raton extends DispositivoEntrada{

    static contadorRatones=0;

    constructor(tipoEntrada,marca){
        super(tipoEntrada,marca);
        this._idRaton = ++Raton.contadorRatones;
        

    }

    toString(){
        return `Tipo Entrada: ${this._tipoEntrada} | Marca: ${this._marca} | ID: ${this._idRaton}`;
    }

}

class Teclado extends DispositivoEntrada{

    static contadorTeclados=0;

    constructor(tipoEntrada,marca){
        super(tipoEntrada,marca);
        this._idTeclado = ++Teclado.contadorteclados;
        

    }

    toString(){
        return `Tipo Entrada: ${this._tipoEntrada} | Marca: ${this._marca} | ID: ${this._idTeclado}`;
    }

}
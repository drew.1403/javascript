function confirmEnding(str, target) {
    let final = str.length;
    let inicio = final - target.length;

    const subArray = str.slice(inicio, final);

    if (subArray === target) {
        return true;  
    }
    return false;
}


console.log(confirmEnding("Abstraction", "A3bstraction"));
    
const squareList = arr => {
    // Only change code below this line
    let integersPositives = arr.filter(element => element >= 0 && Number.isInteger(element));

    let squaredIntegers = integersPositives.map(num => num * num);

    return squaredIntegers;
    // Only change code above this line
  };
  
  const squaredIntegers = squareList([-3.7, -5, 3, 10, 12.5, 7, -4.5, -17, 0.3]);
  console.log(squaredIntegers);
  
  
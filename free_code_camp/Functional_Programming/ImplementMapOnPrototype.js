// The global variable
var s = [23, 65, 98, 5];

Array.prototype.myMap = function(callback) {
  var newArray = [];
  // Only change code below this line
  for (let element of this) {
    newArray.push(callback(element));
    
  }

  //With forEach method
  
  /*
    this.forEach( individualElement => {
    newArray.push(callback(individualElement));

  });

  */
  
  return newArray;
};

var new_s = s.myMap(function(item) {
  return item * 2;
});

console.log(new_s);






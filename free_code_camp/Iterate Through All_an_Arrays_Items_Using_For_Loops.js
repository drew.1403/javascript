function filteredArray(arr, elem) {
    let newArr = [];
    // Only change code below this line
    for (let index = 0; index < arr.length; index++) {
        if(arr[index].indexOf(elem) == -1){
            newArr.push(arr[index]);
        }
        else{
            continue;
        }
        
    }
  
    // Only change code above this line
    return newArr;
  }
  
  console.log(filteredArray([[10, 8, 3], [14, 6, 23], [3, 18, 6]], 18));
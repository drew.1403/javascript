class Producto{

    static contadorProductos = 0;

    constructor(nombre, precio){
        this._nombre = nombre;
        this._precio = precio;
        this._idProducto = ++Producto.contadorProductos;

    }

    get getIdProducto(){
        return this._idProducto;

    }

    get getNombre(){
        return this._nombre;

    }

    set setNombre(new_nombre){
        this._nombre = new_nombre;
    }

    get getPrecio(){
        return this._precio;

    }

    set setPrecio(new_precio){
        this._precio = new_precio;
    }

    toString(){
        return `ID: ${this.getIdProducto} | Nombre: ${this.getNombre} | Precio: ${this.getPrecio}`;
    }



}


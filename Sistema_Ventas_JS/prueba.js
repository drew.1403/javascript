class Producto{

    static contadorProductos = 0;

    constructor(nombre, precio){
        this._nombre = nombre;
        this._precio = precio;
        this._idProducto = ++Producto.contadorProductos;

    }

    get getIdProducto(){
        return this._idProducto;

    }

    get getNombre(){
        return this._nombre;

    }

    set setNombre(new_nombre){
        this._nombre = new_nombre;
    }

    get getPrecio(){
        return this._precio;

    }

    set setPrecio(new_precio){
        this._precio = new_precio;
    }

    toString(){
        return `ID PRODUCTO: ${this.getIdProducto} | NOMBRE: ${this.getNombre} | Precio: ${this.getPrecio}`;
    }



}

class Orden{
    static contadorOrdenes = 0;
    get MAX_PRODUCTOS(){
        return 5;
    }
    constructor(){
        this._idOrden = ++Orden.contadorOrdenes;
        this._productos = [];
        this._contadorProductosAgregados = this._productos.length;

    }

    agregarProducto(new_producto){
        this._productos.push(new_producto);
    }
    
    calcularTotal(){
        var total = 0;

        for (let index = 0; index < this._productos.length; index++) {
            let precio = this._productos[index].getPrecio;
            total+=precio;
            
        }

        return `TOTAL: ${total}$.`;
    }

    mostrarOrden(){
        var txt= `ORDEN --->> ID: ${this._idOrden}. PRODUCTOS:  \n\n`;
        for (let index = 0; index < this._productos.length; index++) {
            txt = txt + this._productos[index].toString() + '\n';
            
        }

        return txt;


    }


}





let p1 = new Producto('papel',34);
let p2 = new Producto('lapiz',11);

let orden1 = new Orden();

orden1.agregarProducto(p2);
orden1.agregarProducto(p1)

console.log(orden1.mostrarOrden());
console.log(orden1.calcularTotal());
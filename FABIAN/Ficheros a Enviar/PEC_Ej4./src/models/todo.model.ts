/**
 * @class Model
 *
 * Manages the data of the application.
 */

export interface todo{
  text:string;
  complete ?: boolean
}

export class Todo {
  id : string;
  text : string;
  complete : boolean;
  constructor({text,complete = false} : todo) {
    this.id = this.uuidv4();
    this.text = text;
    this.complete = complete;
    console.log(text);
  } 

  uuidv4(){
    return ((1e7).toString(10) + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
      (
        parseInt(c) ^
        (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (parseInt(c) / 4)))
      ).toString(16)
    );
  }
}



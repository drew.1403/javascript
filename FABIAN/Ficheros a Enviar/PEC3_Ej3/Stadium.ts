class Stadium {
    private name:string;
    private address:string;
    private location:string;
    private capacity: number;
    private parkingLots: number;
    private created: Date;
    constructor(name:string, address:string, location: string, capacity: number, parkingLots:number, created: Date) {
        this.name = name;
        this.address = address;
        this.location = location;
        this.capacity = capacity;
        this.parkingLots = parkingLots;
        this.created = created;    
    }

    getName():string{
        
        return this.name;

    }

    setName(newName:string):void{
        
    }

    getAddress():string{
        return {} as string;

    }

    setAddress(newAddress:string):void{
        
    }

    getLocation():string{
        return {} as string;

    }

    setLocation(newLocation:string):void{
        
    }

    getCapacity():number{
        return {} as number;

    }

    setCapacity(newCapacity:number):void{
        
    }

    getParkingLots():number{
        return {} as number;

    }

    setParkingLots(newParkingLots:number):void{
        
    }

    getCreated():number{
        return {} as number;

    }

    setCreated(newCreated:number):void{
        
    }

    // addTeam(team:Team):void{

    // }

    // removeTeam(team:Team):void{


    // }

    // getTeam():Team[]{
    //     return [] as Team
    // }

    // toString():string{
    //     return {} as string

    // }




}


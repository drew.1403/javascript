class PlayerAttributes {
    private assists : number;
    private defense : number;
    private energy : number;
    private fg2p : number;
    private fg3p : number;
    private ft : number;
    private height : number;
    private jump : number;
    private rebounds : number;
    private speed : number;
    private weight : number;


    constructor(assists: number, defense : number, energy : number, fg2p : number, fg3p : number, ft : number, height : number, jump : number, rebounds : number, speed : number, weight : number) {
        this.assists = assists;
        this.defense = defense;
        this.energy = energy;
        this.fg2p = fg2p;
        this.fg3p = fg3p;
        this.ft = ft;
        this.height = height;
        this.jump = jump;
        this.rebounds = rebounds;
        this.speed = speed;
        this.weight = weight;
        
    }

    getAssists(): number{
        return {} as number;

    }

    setAssists(assists : number){

    }

    getDefense(): number{
        return {} as number;

    }

    setDefense(defense : number){

    }

    getEnergy(): number{
        return {} as number;

    }

    setEnergy(energy : number){

    }

    getFg2p(): number{
        return {} as number;

    }

    setFg2p(energy : number){

    }

    getFg3p(): number{
        return {} as number;

    }

    setFg3p(energy : number){

    }

    getFt(): number{
        return {} as number;

    }

    setFt(energy : number){

    }

    getRebounds(): number{
        return {} as number;

    }

    setRebounds(energy : number){

    }

    

    getAverage(): number{
        return {} as number;

    }

    getJump(): number{
        return {} as number;

    }

    setJump(jump : number){

    }

    getSpeed(): number{
        return {} as number;

    }

    setSpeed(speed : number){

    }

    getHeight(): number{
        return {} as number;

    }

    setHeight(height : number){

    }

    getWeight(): number{
        return {} as number;

    }

    setWeight(weight : number){

    }




}
class Player extends PlayerAttributes{
    number : number;
    numInternational : number = 0;
    injuredWeeks : number = 0;
    name : string;
    surname : string;
    nick : string;
    birthdate : Date;
    country : Country;
    salary : number;
    cancellationClause : number;
    contractYears : number;
    team : Team;
    
    constructor(name: string, surname: string, nick: string, birthdate: Date,
         country: Country, salary: number, cancellationClause : number,
         contractYears : number, team : Team, number : number,
         numInternational : number, assists: number, defense : number,
         energy : number, fg2p : number, fg3p : number, ft : number, height : number, jump : number, rebounds : number, speed : number, weight : number
         ) {
        super(assists, defense, energy, fg2p , fg3p , ft , height , jump , rebounds , speed , weight );
        this.number = number;
        this.numInternational = numInternational;
        this.name = name;
        this.surname = surname;
        this.nick = nick;
        this.birthdate = birthdate;
        this.country = country;
        this.salary = salary;
        this.cancellationClause = cancellationClause;
        this.contractYears = contractYears;
        this.team = team;
        

        
    }

    getNumber() : number{
        return {} as number;

    }

    setNumber( number : number){
        
    }

    getNumInternational() : number{
        return {} as number;

    }

    setNumInternational( number : number){
        
    }
    getPosition() : Position{
        return {} as Position;

    }

    setPosition( Position : Position){
        
    }

    isInjured() : boolean{
        return {} as boolean;
    }

    getInjuredWeeks() : number{
        return {} as number; 

    }

    setInjuredWeeks( number : number){
        
    }

    getTraining() : Training{
        return {} as Training; 

    }

    setTraining( train : Training){
        
    }

    toString() : string{
        return {} as string;
    }

    
}
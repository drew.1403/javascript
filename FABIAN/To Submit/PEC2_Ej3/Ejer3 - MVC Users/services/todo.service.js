/**
 * @class Service
 *
 * Manages the data of the application.
 */
class UserService {
    constructor() {
      this.users = (JSON.parse(localStorage.getItem("todosUsers")) || []).map(
        user => new User(user)
      );
    }
  
    bindTodoListChanged(callback) {
      
      this.onTodoListChanged = callback;
    }
  
    _commit(users) {
      this.onTodoListChanged(users);
      localStorage.setItem("todosUsers", JSON.stringify(users));
    }
  
    addTodo({name, email, address, phone}) {
      this.users.push(new User({ name, email, address, phone}));
  
      this._commit(this.users);
    }
  
    editTodo(_id, userEdited) {
      
      this.users = this.users.map(user => {

        if (user.id === _id) {
          console.log(user);
          user.name = userEdited.name;
          user.email = userEdited.email;
          user.address = userEdited.address;
          user.phone = userEdited.phone;
          
          return user;
          
        }
        else{
          return user;
        }
        

      }
        
      );
  
      this._commit(this.users);
    }
  
    deleteTodo(_id) {
      this.users = this.users.filter(({ id }) => id !== _id);
  
      this._commit(this.users);
    }
  
    toggleTodo(_id) {
      
      this.users = this.users.map(user =>
        user.id === _id ? new User({ ...user, complete: !user.complete }) : user
      );
  
      this._commit(this.users);
    }
    toggleAll(option){

      
        this.users = this.users.map(user => new User({ ...user, complete: option }));
        
    
        this._commit(this.users);
        
      

    }

    deleteByCheckbox(){
      this.users = this.users.filter( user => user.complete !== true);
      
      this._commit(this.users);


    }

    searchUser(_id){
      const userfiltered = this.users.filter( user => user.id === _id);
      
      return userfiltered[0];
    }
  }
  
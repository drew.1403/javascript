/**
 * @class View
 *
 * Visual representation of the model.
 */
class TodoView {
    constructor() {
      this.tableUsers = this.getElement("#tableUsers");
      
  
      this._temporaryTodoText = "";
      
    }
  
    get _todoText() {
      return this.input.value;
    }
  
    _resetInput() {
      this.input.value = "";
    }
  
    createElement(tag, className) {
      const element = document.createElement(tag);
  
      if (className) element.classList.add(className);
  
      return element;
    }
  
    getElement(selector) {
      const element = document.querySelector(selector);
  
      return element;
    }
  
    displayTodos(users) {
      // Delete all nodes
      while (this.tableUsers.firstChild) {
        this.tableUsers.removeChild(this.tableUsers.firstChild);
      }
  
      // Show default message
      if (users.length === 0) {
        const tr = this.createElement("tr");
        tr.textContent = "Nothing to do! Add a task?";
        this.tableUsers.append(tr);
      } else {
        // Create nodes
        users.forEach(user => {
          const tr = this.createElement("tr");
          tr.id = user.id;
  
          const tdcheckbox = this.createElement("td");

          const span = this.createElement("span","custom-checkbox");
        
          const checkbox = this.createElement("input");
          checkbox.type = "checkbox";
          checkbox.checked = user.complete;
          checkbox.value = "1";
          checkbox.id = user.id;
          const label = this.createElement('label');
          label.for = user.id;
          span.append(checkbox,label);
          tdcheckbox.append(span);

          const name = this.createElement("td");
          name.textContent = user.name;

          const email = this.createElement("td");
          email.textContent = user.email;

          const address = this.createElement("td");
          address.textContent = user.address;

          const phone = this.createElement("td");
          phone.textContent = user.phone;

          const options = this.createElement("td");

          const anchorEdit = this.createElement("a","edit");
          anchorEdit.setAttribute("data-toggle","modal");
          const iconOfanchorEdit = this.createElement("i","material-icons");
          iconOfanchorEdit.setAttribute("data-toggle", "tooltip");
          iconOfanchorEdit.setAttribute("title", "Edit");
          iconOfanchorEdit.innerHTML = "&#xE254;";
          
          anchorEdit.append(iconOfanchorEdit);

          const anchorDelete = this.createElement("a","delete");
          anchorDelete.setAttribute("data-toggle","modal");
          const iconOfanchorDelete = this.createElement("i","material-icons");
          iconOfanchorDelete.setAttribute("data-toggle", "tooltip");
          iconOfanchorDelete.setAttribute("title", "Delete");
          
          iconOfanchorDelete.innerHTML = "&#xE872;";
          
          
          anchorDelete.append(iconOfanchorDelete);


          options.append(anchorEdit,anchorDelete);

          tr.append(tdcheckbox, name, email, address, phone, options);
  
          // Append nodes
          this.tableUsers.append(tr);
        });
      }
  
      // Debugging
      console.log(users);
    }
  
    
    bindAddTodo(handler) {
      
      document.getElementById("addEmployeeModal").addEventListener("submit", event => {
        event.preventDefault();
        const name = event.target[1].value;
        const email = event.target[2].value;
        const address = event.target[3].value;
        const phone = event.target[4].value;
        handler({name,email,address,phone});
        $("#addEmployeeModal").modal("hide");
        console.log("hidee");


      });
    }
  
    bindDeleteTodo(handler) {
      this.tableUsers.addEventListener("click", event => {
        
        
        if (event.target.parentElement.className === "delete") {
          
          const id = event.target.parentElement.parentElement.parentElement.id;
          const modal = document.getElementById("deleteEmployeeModal");
          $("#deleteEmployeeModal").modal("show");
          
          modal.addEventListener("submit",event => {
            $("#deleteEmployeeModal").modal("hide");
            
            handler(id);
          });
        }
      });
    }
  
    bindDeleteByCheckbox(handler){
      document.getElementById("deleteEmployeeModal").addEventListener("submit", event => {
        event.preventDefault();
        handler();
        

      });
    }
  
    bindToggleTodo(handler) {
      this.tableUsers.addEventListener("change", event => {
        
        if (event.target.type === "checkbox") {
          
          const id = event.target.parentElement.parentElement.parentElement.id;
    
  
          handler(id);
        }
      });
    }

    bindToggleAll(handler){
      var checkbox = $('table tbody input[type="checkbox"]');
      $("#selectAll").click(function(){
        if(this.checked){
          checkbox.each(function(){
            this.checked = true;                        
          });
          handler(true);
        } else{
          checkbox.each(function(){
            this.checked = false;                        
          });
          handler(false);
        } 
      });
 
    }

  bindEditAll(search,handler){
    this.tableUsers.addEventListener("click", event => {
        
      if (event.target.parentElement.className === "edit") {
        
        const id = event.target.parentElement.parentElement.parentElement.id;
        const userFiltered = search(id);
        
        const modal = document.getElementById("editEmployeeModal");
        $('#name').val(userFiltered.name);
        $('#email').val(userFiltered.email);
        $('#address').val(userFiltered.address);
        $('#phone').val(userFiltered.phone);

        $("#editEmployeeModal").modal("show");
        
        modal.addEventListener("submit", event => {
          event.preventDefault();
          const userEdited = {};
          userEdited['name'] = $('#name').val();
          userEdited['email'] = $('#email').val();
          userEdited['address'] = $('#address').val();
          userEdited['phone'] = $('#phone').val();
          handler(id, userEdited);
          $("#editEmployeeModal").modal("hide");

        });

        
       
        
      }
    });

  }

  

  }
  
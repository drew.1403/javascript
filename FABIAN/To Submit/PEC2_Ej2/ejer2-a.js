/* 
La función findOne recibe como parámetros:

1) Una lista de usuarios con sus roles (Array de objetos)
2) Un Objeto con key y value, que se usara posteriormente para la busqueda de usuario.
3) Un Objeto que contiene las callbacks onSuccess(usada en caso de encontrar el usuario) y 
onError(en caso de no encontrar usuarios)

En el interior de findOne, tenemos un setTimeout para simular una operacion asincrona de 
busqueda usuario,esta función tiene la variable 'element ' que contiene el resultado de la 
busqueda (list.find) del usuario en base
a los key y value proporcionados a findOne()

Luego se evalúa si element contiene el objeto con el usuario encontrado, en ese caso se 
ejecuta:

callback onSuccess: dicha callback recibe como parametro la variable element, y 
posteriormente imprime el nombre del usuario en base a la key : name de element.

Si element no contiene el objeto,(undefined), se ejecuta la callback:

onError: recibe como parámetro un objeto con un mensaje de error(fallo en la busqueda), y se
encarga de imprimir dicho mensaje.


Es asi como findOne opera de la mano de las callbacks donde la callback onSuccess imprime 
el usuario encontrado y en caso de no encontrarse el usuario dentro de findOne 
(element = undefined) la callback onError se encarga de imprimir el mensaje de error.









*/
const findOne = (list, { key, value }) => { 
    return new Promise((resolve,reject) => {
        setTimeout(() => {
            const element = list.find(element => element[key] === value);
            element ? resolve(element) : reject({ msg: 'ERROR: Element Not Found' });
        }, 2000);
    
    });
    
        
};
    
const users = [
    {
        name: 'Carlos',
        rol: 'Teacher'
    },
    {
        name: 'Ana',
        rol: 'Boss'
    }
];



async function resultSearch(list, { key, value }){ //Se define la funcion asincrona
    // se usará try/catch para el manejo del resultado de la promesa
    try {
        const resultFind1 = findOne(list, {key, value});
        const resultFind2 = findOne(list,{key,value});
        const result = await Promise.all( [resultFind1,resultFind2] );
        console.log(result);
    } catch (error) {
        console.log(error.msg);
        
    }
    
    // await se encarga de esperar el resultado de la promesa.
    
    
    
}

resultSearch(users,{ key: 'name', value: 'Carlos' });
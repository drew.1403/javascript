function multiplyBy10(array) {
  // your code here
  return array.map(item => item * 10);
}

function shiftRight(array) {
  // your code here
  let shiftRighted = array.map((value,index,array) => {
    let posFinal = array.length - 1;
    let aux = array[index];

    array[index] = array[posFinal];

    array[posFinal] = aux;

    return array[index];
  });

  return shiftRighted;
}

function onlyVowels(array) {
  // your code here

  return array.map(word => {

    let arrayWord = word.split('');

    let vowels = '';

    for (let index = 0; index < arrayWord.length; index++) {
      if (arrayWord[index] === 'a' || arrayWord[index] === 'e' || arrayWord[index] === 'i' || arrayWord[index] === 'o' || arrayWord[index] === 'u') {
        vowels += arrayWord[index];
      }
      
    }

    return vowels;

  });
}

function doubleMatrix(array) {
  // your code here
  return array.map(number => {

      let doubleArray = number.map( element => element * 2);

      return doubleArray;
  });
}

module.exports = {
  multiplyBy10,
  shiftRight,
  onlyVowels,
  doubleMatrix
};







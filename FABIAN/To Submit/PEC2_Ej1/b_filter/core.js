function onlyEven(array) {
  // your code here
  return array.filter(element => element % 2 ==0);
}

function onlyOneWord(array) {
  // your code here
  return array.filter(word => !word.includes(' '));

}

function positiveRowsOnly(array) {
  // your code here
  return array.filter(row => {

    for (const element of row) {

      if (element < 0 || Number.isInteger(element) === false) {
        return false; 
      }

    }
    return row;


  });
}

function allSameVowels(array) {
  // your code here
  return array.filter(word => {

    let regexpVowels = /[aeiou]/g;
    let onlyVowels = word.match(regexpVowels);
    
    let vowel = onlyVowels[0];

    for (const element of onlyVowels) {
      if (element != vowel) {
        return false;
        
      }
    }
    return word;
  }
  );
}

module.exports = {
  onlyEven,
  onlyOneWord,
  positiveRowsOnly,
  allSameVowels
};



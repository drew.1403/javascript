function sum(array) {
  // your code here
  return array.reduce((acumulador,element) => acumulador + element);
}

function productAll(array) {
  // your code here
  return array.reduce((acumulador, row) => {

    return acumulador * row.reduce((acumulador,element) => acumulador * element,1);

  },1);
}

function objectify(array) {
  // your code here
  return array.reduce((obj, element) => {
    obj[element[0]] = element[1];
    return obj;
  },{});
}

function luckyNumbers(array) {
  // your code here
  return array.reduce((cadena, element, index, array) => {

    let posFinal = array.length - 1;

    if (index === posFinal) {
      return cadena += ('and ' + element);
    }
    return cadena += (element + ', ');

  },'Your lucky numbers are: ');
}

module.exports = {
  sum,
  productAll,
  objectify,
  luckyNumbers
};



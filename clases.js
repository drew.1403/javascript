/* 
class Persona{
    constructor(nombre, genero, ocupacion){
        this._nombre = nombre;
        this._genero = genero;
        this._ocupacion = ocupacion;
    }

    get obtenerNombre(){
        return this._nombre;
    }

    get obtenerGenero(){
        return this._genero;
    }
    get obtenerOcupacion(){
        return this._ocupacion;
    }

    set setNombre(new_name){
        this._nombre= new_name;
    }

    set setGenero(new_genero){
        this._genero= new_genero;
    }
    set setOcupacion(new_ocupacion){
        this._ocupacion= new_ocupacion;
    }
}

let gory = new Persona('mbappi','m','programador');
console.log(gory.obtenerNombre);
gory.setNombre= 'leo';
console.log(gory);

*/

class Persona{

    constructor(nombre, apellido){
        this._nombre = nombre;
        this._apellido = apellido;
    }

    get getNombre(){
        return this._nombre;
    }
    set setNombre(new_nombre){
        this._nombre = new_nombre;
    }

    get getApellido(){
        return this._apellido;
    }

    set setApellido(new_apellido){
        this._apellido = new_apellido;
    }

    

    toString(){
        return 'Nombre: ' + this._nombre + ' | Apellido: ' + this._apellido;
    }


}

class Empleado extends Persona{

    constructor(departamento,nombre,apellido){
        super(nombre, apellido);
        this._departamento = departamento;
    }
    get getDepartamento(){
        return this._departamento;
    }
    set setDepartamento(new_departamento){
        this._departamento = new_departamento;
    }

    toString(){
        return super.toString() + ' | Departamento: ' + this._departamento;
    }


}

let employ = new Empleado('caja','cristhian','nodal');
console.log(employ.getApellido);
employ.setNombre = 'gory';
console.log(employ.toString());
class Persona{
    constructor(nombre, apellido){
        this._nombre = nombre;
        this._apellido = apellido;

    }

    get nombre(){
        return this._nombre;
    }

    set nombre(new_name){
        this._nombre = new_name;
    }

    get apellido(){
        return this._apellido;
    }

    set apellido(new_apellido){
        this._apellido = new_apellido;
    }
}